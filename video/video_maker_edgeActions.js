
(function($,Edge,compId){var Composition=Edge.Composition,Symbol=Edge.Symbol;
//Edge symbol: 'stage'
(function(symbolName){Symbol.bindTriggerAction(compId,symbolName,"Default Timeline",0,function(sym,e){sym.stop();});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_Stage}","click",function(sym,e){});
//Edge binding end
Symbol.bindSymbolAction(compId,symbolName,"creationComplete",function(sym,e){var video=document.getElementById('Stage_campina');setInterval(function(){sym.play(video.currentTime*1000);},1000);});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_pause_hover}","click",function(sym,e){});
//Edge binding end
Symbol.bindElementAction(compId,symbolName,"${_playPause}","click",function(sym,e){var video=document.getElementById('Stage_campina');if(sym.$("campina")[0].paused){sym.play(video.currentTime*1000);sym.$("campina")[0].play();sym.$("pause_hover").hide();}
else{sym.$("campina")[0].pause();sym.stop();sym.$("pause_hover").show();}});
//Edge binding end
})("stage");
//Edge symbol end:'stage'
})(jQuery,AdobeEdge,"EDGE-10235114");