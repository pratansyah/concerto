<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()	{
		$this->data['body_class'] = 'home';
		$this->data['content'] = 'home';
		$this->data['title'] = 'Campina tasty moment';
		$this->load->view('main', $this->data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */