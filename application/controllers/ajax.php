<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('user_mdl');
		$this->load->model('video_mdl');
		$this->load->model('likes_mdl');
		$this->load->config('values');
	}
	public function index()	{
		echo "There's nothing here, keep going with your life.";
	}

	public function login($facebook_id, $name, $token) {
		//Used both for registered and unregistered user.
		$status = $this->user_mdl->add_user($facebook_id, urldecode($name));
		if($status) $this->session->set_userdata(array('user_id' => $facebook_id, 'token' => $token));
		echo json_encode(array('status' => $status));
	}

	public function upload() {
		$files = $this->input->post('file');
		$total =  count($files);
		$pictures = array();
		for($i = 0; $i < $total; $i++){
	    	$base64img = str_replace('data:image/jpeg;base64,', '', $files[$i]);
		    $data = base64_decode($base64img);
		    $filename = md5($this->session->userdata('user_id').time().$i) .'.jpg';
		    $file = $this->config->item('upload_real_path') . '/'. $filename;
		    if(file_put_contents($file, $data)) {
		    	$picture = new stdClass();
		    	$picture->filename = $filename;
		    	$picture->sequence = $i;
		    	$pictures[] = $picture;
		    }
		}

		$title = $this->input->post('title');
		$kata_manis = $this->input->post('kata_manis');
		$kata_semangat = $this->input->post('kata_semangat');
		$rasa_persahabatan = $this->input->post('rasa_persahabatan');

		$video_id = $this->video_mdl->add_video($this->session->userdata('user_id'), $pictures, $title, $kata_manis, $kata_semangat, $rasa_persahabatan);
		echo json_encode(array('video_id' => $video_id));
	}

	public function like() {
		$video_id = $this->input->post('video_id');
		$user_id = $this->session->userdata('user_id');
		echo json_encode(array('status' => $this->likes_mdl->like($video_id, $user_id)));
	}
}

/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */