<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class View extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('video_mdl');
		$this->load->library('pagination');
		$this->_per_page = 12;

		$this->pagination_config['base_url'] = base_url()."view/all/";
		$this->pagination_config['uri_segment'] = 3;
		$this->pagination_config['per_page'] = $this->_per_page;
		$this->pagination_config['full_tag_open'] = '<nav class="pagination"> <ul>';
		$this->pagination_config['full_tag_close'] = "</nav> </ul>";
		$this->pagination_config['first_tag_open'] = false;
		$this->pagination_config['first_tag_close'] = false;
		$this->pagination_config['use_page_numbers'] = true;

		$this->pagination_config['next_link'] = "next";
		$this->pagination_config['next_tag_open'] = '<li class="prev button">';
		$this->pagination_config['next_tag_close'] = '</li>';

		$this->pagination_config['cur_tag_open'] = '<li><a href="#">';
		$this->pagination_config['cur_tag_close'] = '</a></li>';

		$this->pagination_config['prev_link'] = "prev";
		$this->pagination_config['prev_tag_open'] = '<li class="next button">';
		$this->pagination_config['prev_tag_close'] = '</li>';

		$this->pagination_config['num_tag_open'] = '<li>';
		$this->pagination_config['num_tag_close'] = '</li>';
		$this->pagination_config['last_link'] = false;
		// $this->pagination_config['page_query_string'] = true;
	}
	public function index()	{
		redirect('/view/all');
	}

	public function all($page=1) {
		$query = $order = "";
		$limit = $this->_per_page;
		$offset = ($this->_per_page * $page) - $this->_per_page;
		if($this->input->get()) {
			extract($this->input->get());
			$query = ($search_query) ? $search_query : "";
			$order = ($sort != 'null') ? $sort : "";
			$this->pagination_config['suffix'] = '?'.http_build_query($_GET, '', "&");
		}
		$this->data['body_class'] = 'gallery';
		$this->data['content'] = 'gallery';
		$this->data['title'] = 'Submission gallery';
		$this->data['query'] = $query;
		$user_id = ($this->session->userdata("user_id") != false) ? $this->session->userdata("user_id") : -1;
		$this->data['videos'] = $this->video_mdl->get_videos(($query != "") ? array('title' => $query) : "", $order, $limit, $offset, $user_id);
		// var_dump($this->data['videos']);die();
		$this->pagination_config['total_rows'] = count($this->video_mdl->get_videos(($query != "") ? array('title' => $query) : ""));
		$this->pagination->initialize($this->pagination_config);
		$this->data['pagination'] = $this->pagination->create_links();
		$this->load->view('main', $this->data);
	}

	public function single($video_id) {
		$this->data['body_class'] = 'single-gallery';
		$this->data['content'] = 'single';
		$this->data['video'] = $this->video_mdl->get_video($video_id);
		$this->data['title'] = $this->data['video']->title;
		// var_dump($this->data['video']);die();
		$this->data['videoid'] = $video_id;
		// var_dump($this->data['video']);die();
		$this->load->view('main', $this->data);
	}
}