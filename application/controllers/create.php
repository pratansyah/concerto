<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Create extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->config('values');
		$this->load->model('video_mdl');
	}

	public function index()	{
		$this->data['body_class'] = 'create-video instructions overlayed';
		$this->data['content'] = 'create';
		$this->data['title'] = 'Create video';
		$this->load->view('main', $this->data);
	}

	public function review($video_id) {
		$this->data['body_class'] = 'review-video';
		$this->data['content'] = 'review';
		$this->data['title'] = 'Review video';
		$this->data['video'] = $this->video_mdl->get_video($video_id);
		$this->load->view('main', $this->data);
	}
}

/* End of file create.php */
/* Location: ./application/controllers/create.php */