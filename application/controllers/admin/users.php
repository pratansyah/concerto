<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('user_mdl');
		$this->load->model('video_mdl');
		$this->data['navigation'] = 'users';
		if(!$this->session->userdata('admin')) redirect('/admin');
	}
	public function index()	{
		$this->data['users'] = $this->user_mdl->get_users();
		$this->data['content'] = 'users';
		$this->load->view('admin/main', $this->data);
	}

	public function detail($user_id) {
		$this->data['content'] = 'users-single';
		$this->data['video'] = $this->video_mdl->get_videos_by_user($user_id);
		$this->load->view('admin/main', $this->data);
	}

	
}

/* End of file users.php */
/* Location: ./application/controllers/admin/users.php */