<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Submissions extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('video_mdl');
		$this->data['navigation'] = 'submissions';
		if(!$this->session->userdata('admin')) redirect('/admin');
	}
	public function index()	{
		$this->data['videos'] = $this->video_mdl->get_videos();
		$this->data['content'] = 'submissions';
		$this->load->view('admin/main', $this->data);
	}

	public function detail($video_id) {
		$this->data['content'] = 'submissions-single';
		$this->data['video'] = $this->video_mdl->get_video($video_id);
		$this->load->view('admin/main', $this->data);
	}

	
}

/* End of file submissions.php */
/* Location: ./application/controllers/admin/submissions.php */