<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
		parent::__construct();
	}
	public function index()	{
		if($this->input->post()) {
			extract($this->input->post());
			if($username == "admin" && $password == "password") {
				$this->session->set_userdata(array('admin' => true));
				redirect('admin/submissions');
			}
		}
		// $this->data['content'] = 'login';
		$this->load->view('admin/login');

	}

	
}

/* End of file login.php */
/* Location: ./application/controllers/admin/login.php */