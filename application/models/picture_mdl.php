<?php
	class Picture_mdl extends CI_Model {
		private $_conn;
		private $_table = 'pictures';

		function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function add_picture($video_id, $filename, $sequence){
			$status = $this->_conn->save(array('video_id' => $video_id, 'filename' => $filename, 'sequence' => $sequence), $this->_table);
			return $status;
		}

		public function delete_pictures($video_id) {
			return $this->_conn->remove($this->_table, array('video_id' => $video_id));
		}

		public function get_pictures($video_id) {
			return $this->_conn->retrieve($this->_table, array('video_id' => $video_id));
		}

		public function get_cover($video_id) {
			$pictures = $this->_conn->retrieve($this->_table, array('video_id' => $video_id));
			return (count($pictures) > 1)? $pictures[0] : $pictures;
		}

	}
?>