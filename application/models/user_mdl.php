<?php
	class User_mdl extends CI_Model {
		private $_conn;
		private $_table = 'user';

		function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			// $this->load->model('video_mdl');
			$this->_conn = new Conn();
		}

		public function add_user($user_id, $name){
			$status = TRUE;
			if(!$this->user_exist($user_id)){
				$status = $this->_conn->save(array('user_id' => $user_id, 'name' => $name), $this->_table);
			}
			return $status;
		}

		public function user_exist($user_id){
			$status = $this->_conn->search($this->_table, array('user_id' => $user_id));
			if($status != FALSE) return TRUE;
			else return FALSE;
		}

		public function get_username($user_id) {
			return $this->_conn->retrieve($this->_table, array('user_id' => $user_id), 'name');
		}

		public function get_users() {
			$query = "select u.user_id, u.name, (select count(v.video_id) from video v where v.user_id=u.user_id) as total from user u";
			$users = $this->_conn->nativeQuery($query);
			return $users;
		}

	}
?>