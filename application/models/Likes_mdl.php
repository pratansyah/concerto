<?php
	class Likes_mdl extends CI_Model {
		private $_conn;
		private $_table = 'likes';

		function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->_conn = new Conn();
		}

		public function get_like($video_id, $user_id) {
			$result = $this->_conn->retrieve($this->_table, array("video_id" => $video_id, "user_id" => $user_id));
			return ($result) ? true : false;
		}

		public function get_total_likes($video_id) {
			$result = $this->_conn->retrieve($this->_table, array("video_id" => $video_id));
			return count($result);
		}

		public function like($video_id, $user_id) {
			if(count($this->_conn->retrieve($this->_table, array('video_id' => $video_id, 'user_id' => $user_id))) != 0 ) return false;
			return $this->_conn->save(array('video_id' => $video_id, 'user_id' => $user_id), $this->_table);
		}

	}
?>