<?php
	class Video_mdl extends CI_Model {
		private $_conn;
		private $_table = 'video';

		function __construct() {
			parent::__construct();

			$this->load->library('Conn');
			$this->load->model('picture_mdl');
			$this->load->model('user_mdl');
			$this->load->model('likes_mdl');
			$this->_conn = new Conn();
		}

		public function add_video($user_id, $pictures = array(), $title, $kata_manis = "", $kata_semangat = "", $rasa_persahabatan = ""){
			$status = TRUE;
			$video_id = $this->_conn->save(array('user_id' => $user_id, 'title' => $title, 'kata_manis' => $kata_manis, 'kata_semangat' => $kata_semangat, 'rasa_persahabatan' => $rasa_persahabatan), $this->_table, TRUE);
			foreach($pictures as $picture){
				$status = $this->picture_mdl->add_picture($video_id, $picture->filename, $picture->sequence);
				if(!$status) break;
			}
			return $video_id;
		}

		public function delete_video($video_id){
			$status = $this->_conn->remove($this->_table, array('video_id' => $video_id));
			if($status) $this->picture_mdl->delete_pictures($video_id);

			return $status;
		}

		public function get_videos($where = "", $orderby="", $limit = "", $offset = "", $user_id = null){
			$orderby = ($orderby == "") ? "video_id" : $orderby;
			$order = ($orderby == "video_id") ? "DESC" : "ASC";
			$videos = $this->_conn->search($this->_table, $where, "", "", $orderby, $order, $limit, $offset);
			foreach($videos as $video) {
				$video->cover = $this->picture_mdl->get_cover($video->video_id);
				if($user_id == null) {
					$video->total_likes = $this->likes_mdl->get_total_likes($video->video_id);
					$video->username = $this->user_mdl->get_username($video->user_id)->name;
				} else {
					$video->like = $this->likes_mdl->get_like($video->video_id, $user_id);
					$video->total_likes = $this->likes_mdl->get_total_likes($video->video_id);
					$video->username = $this->user_mdl->get_username($video->user_id)->name;
				}
			}
			// var_dump($videos);die();
			return $videos;
		}

		public function get_video($video_id){
			$video = $this->_conn->retrieve($this->_table, array('video_id' => $video_id));
			$video->pictures = $this->picture_mdl->get_pictures($video_id);
			$video->cover = $this->picture_mdl->get_cover($video->video_id);
			return $video;
		}

		public function get_video_counts($user_id=-1) {
			return ($user_id != -1) ? count($this->_conn->retrieve($this->_table, array('user_id' => $user_id))) : count($this->_conn->retrieve($this->_table));
		}

		public function get_videos_by_user($user_id) {
			$result['username'] = $this->user_mdl->get_username($user_id)->name;
			$result['submissions'] = $this->_conn->retrieve($this->_table, array("user_id" => $user_id));
			foreach($result['submissions'] as $video) {
				$video->total_likes = $this->likes_mdl->get_total_likes($video->video_id);
			}
			return $result;
		}

	}
?>