<main id="content" >
    <div class="container">
        <div class="row wrapper">
            <div class="functional-buttons">
                <form method="GET" id="search-form">
                    <div class="search">
                        <input type="search" class="search-field" name="search_query" <?php echo ($query != "") ? "value='$query'" : ''; ?> placeholder="Cari">
                        <input type="submit">
                    </div>
                    <select id="filters" name="sort">
                        <option value="null">Urutkan</option>
                        <option value="title">Judul</option>
                        <option value="created">Tanggal</option>
                    </select>
                </form>
            </div>
            <div class="inner isotope">
                <?php foreach($videos as $video): ?>
                    <div class="col-3 thumbnail-gallery red " data-category="red">
                        <div class="inner">
                            <h3 class="title"><?php echo $video->title; ?></h3>
                            <a href="<?php echo base_url()."view/single/".$video->video_id; ?>" >
                                <figure>
                                    <img src="<?php ($video->cover) ? upload_url($video->cover->filename): assets_url('img/no-image-available.jpg'); ?>" alt="<?php echo $video->title; ?>" alt="">
                                </figure>
                            </a>
                            <div class="caption">
                                <div class="name"><?php echo $video->username; ?></div>
                                <div class="likes <?php echo ($video->like) ? "liked" : ""; ?>" data-video-id="<?php echo $video->video_id; ?>" ><?php echo $video->total_likes; ?></div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                
            </div>
            <?php echo $pagination; ?>
        </div>
        <div class="scroll">
            scroll
            <img class="arrow-top" src="<?php assets_url('img/arrow.png'); ?>" alt="">
            <img class="arrow-bottom" src="<?php assets_url('img/arrow2.png'); ?>" alt="">
        </div>
    </div>
</main>