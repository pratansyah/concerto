<main id="content" >
    <div class="container">
        <div class="wrapper">
            <div class="video-container pull-left">
                <div class="inner">
                    <iframe id="video" src="<?php echo base_url()."video/video_maker.html"; ?>" width="480" height="300"></iframe>
                </div>
            </div>
            <div class="fb-comment pull-right" style="background-color: #ffffff;">
                <div class="fb-comments" data-href="<?php echo base_url(); ?>/view/single/<?php echo $videoid; ?>" data-width="380" data-numposts="3" data-colorscheme="light"></div>
            </div>
        </div>
    </div>
</main>