<main id="content" >
    <div class="container">
        <div class="row">
            <p class="text">Rangkai foto kisah manis persahabatanmu ke dalam video clip Tasty Moment dan kirimkan untuk sahabatmu </p>
        </div>
        <div class="clear wrapper">
            <form action="<?php echo base_url()."create/review"; ?>" method="POST" id="form">
                <div class="inputs pull-left">
                    <div class="row">
                        <span class="instruction one">Berikan judul unik untuk video manis persahabatanmu</span>
                        <input id="title" type="text" class="input-text" placeholder="Ketik judul video">
                        <div class="char-counter"><div class="charNum">20</div> characters left</div>
                    </div>
                    <div class="row">
                        <span class="instruction two">Pilih Tema</span>
                        <select class="select-category">
                            <option value="0">Pilih Kategori</option>
                            <option value="1">Tasty Moment with Friends</option>
                        </select>
                    </div>
                    <div class="row">
                        <span class="instruction x">Berikan kata – kata manis untuk sahabatmu </span>
                        <span class="instruction three">Masukan Rangkaian Kata-Kata Manismu Dalam Video dan Share di Facebook dan Twittermu</span>
                        <input id="kata_manis" id="title" type="text" onkeyup="countChar(this)" class="input-text" placeholder="Pilih kata kata manis untuk sahabatmu">
                        <div class="char-counter"><div class="charNum">20</div> characters left</div>
                    </div>
                    <input id="kata_semangat" type="text" onkeyup="countChar(this)" class="input-text" placeholder="Buat kata - kata yang menyemangati sahabatmu">
                    <div class="char-counter"><div class="charNum">50</div> characters left</div>

                    <input id="rasa_persahabatan" type="text" onkeyup="countChar(this)" class="input-text" placeholder="Tuangkan rasa persahabatanmu ke dalam satu kalimat">
                    <div class="char-counter"><div class="charNum">50</div> characters left</div>
                    
                </div>
                <div class="photos-uploader pull-right">
                    <a href="#" class="fb-upload" title="Silakan ambil fotomu dari galeri Facebook atau Desktop
">facebook upload</a>
                    <input type="file" class="desktop" name="photos" id="userPhotos" title="Silakan ambil fotomu dari galeri Facebook atau Desktop
" multiple>
                    <div class="row">
                        <span class="instruction four">
                            <img src="<?php assets_url('img/arrow4.png'); ?>" alt="">
                        </span>
                    </div>
                    <div class="row">
                        <span class="instruction five">
                            <img src="<?php assets_url('img/arrow5.png'); ?>" alt="">
                        </span>
                    </div>
                    <div class="row">
                        <a href="#" class="btn fb"><img src="<?php assets_url('img/fb.png'); ?>" alt=""></a>
                        <a href="#" class="btn twt"><img src="<?php assets_url('img/twt.png'); ?>" alt=""></a>
                        <div class="gallery-upload">
                            <span class="instruction six">
                                <img src="<?php assets_url('img/arrow6.png'); ?>" alt="">
                            </span>
                            <div class="photos">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row functional-buttons">
                    <div class="button tutorial">cara buat</div>
                    <div class="button next">Selesai</div>
                </div>
            </form>
        </div>
    </div>
</main>