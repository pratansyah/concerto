<main id="content" >
    <div class="container">
        <div class="wrapper clear">
            <div class="col-2">
                <div class="cone">
                    <img src="<?php assets_url('img/splash1.png'); ?>" alt="" class="splash one" data-xrange="30" data-yrange="10">
                    <img src="<?php assets_url('img/splash2.png'); ?>" alt="" class="splash two" data-xrange="10" data-yrange="20">
                    <img src="<?php assets_url('img/splash3.png'); ?>" alt="" class="splash three" data-xrange="40" data-yrange="20">

                    <img src="<?php assets_url('img/ice-cream4.png'); ?>" alt="">
                </div>
                <a href="<?php echo base_url()."create/" ?>" class="button">buat lagi</a>
            </div>
            <div class="col-8">
                <div class="video-container">
                    <div class="inner">
                        <iframe src="<?php echo base_url()."video/video_maker.html"; ?>" width="480" height="300"></iframe>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="cone2">
                    <img src="<?php assets_url('img/ice-cream5.png'); ?>" alt="">
                    <img src="<?php assets_url('img/splash4.png'); ?>" alt="" class="splash four" data-xrange="30" data-yrange="10">
                    <img src="<?php assets_url('img/splash5.png'); ?>" alt="" class="splash five" data-xrange="10" data-yrange="20">
                    <img src="<?php assets_url('img/splash6.png'); ?>" alt="" class="splash six" data-xrange="10" data-yrange="30">
                    <img src="<?php assets_url('img/splash7.png'); ?>" alt="" class="splash seven" data-xrange="30" data-yrange="10">
                    <img src="<?php assets_url('img/splash8.png'); ?>" alt="" class="splash eight" data-xrange="10" data-yrange="25">
                    <img src="<?php assets_url('img/splash9.png'); ?>" alt="" class="splash nine" data-xrange="40" data-yrange="20">
                    <img src="<?php assets_url('img/splash10.png'); ?>" alt="" class="splash ten" data-xrange="10" data-yrange="5">
                </div>
                <a href="#" class="button share">share</a>
            </div>
        </div>
        
    </div>
</main>