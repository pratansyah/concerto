<div class="text">
    <p>Indahnya kisah manis, canda tawa, diiringi lezatnya Campina Concerto, warnai moment persahabatanmu!</p>
    <p>Login untuk buat video <span>Tasty Moment</span> dan kirimkan untuk sahabatmu.</p>
    <a href="#" onclick="login_facebook()" class="button">Login dengan Facebook</a>
</div>
<main id="content" >
    <div class="container">
        <div class="ice-creams">
            <div class="wrap1">
                <img class="ice x" src="<?php assets_url('img/top-ice1.png'); ?>" alt="">
                <img class="ice one" src="<?php assets_url('img/ice-cream1.png'); ?>" alt="">
                <img class="splash1" src="<?php assets_url('img/splash4.png'); ?>" alt="">
                <img class="splash2" src="<?php assets_url('img/splash7.png'); ?>" alt="">
                <img class="splash3" src="<?php assets_url('img/splash6.png'); ?>" alt="">
            </div>
            <div class="wrap2">
                <img class="ice x" src="<?php assets_url('img/top-ice2.png'); ?>" alt="">
                <img class="ice two" src="<?php assets_url('img/ice-cream2.png'); ?>" alt="">
                <img class="splash1" src="<?php assets_url('img/splash8.png'); ?>" alt="">
                <img class="splash2" src="<?php assets_url('img/splash9.png'); ?>" alt="">
                <img class="splash3" src="<?php assets_url('img/splash10.png'); ?>" alt="">
            </div>
            <div class="wrap3">
                <img class="ice x" src="<?php assets_url('img/top-ice3.png'); ?>" alt="">
                <img class="ice three" src="<?php assets_url('img/ice-cream3.png'); ?>" alt="">
                <img class="splash1" src="<?php assets_url('img/sp1.png'); ?>" alt="">
                <img class="splash2" src="<?php assets_url('img/sp2.png'); ?>" alt="">
                <img class="splash3" src="<?php assets_url('img/sp3.png'); ?>" alt="">
            </div>
        </div>

        <div class="right-bg">
            <div class="strawberry"></div>
        </div>
        <div class="monster1"><img src="<?php assets_url('img/monster1.png'); ?>" alt=""></div>
        <div class="monster2"><img src="<?php assets_url('img/monster2.png'); ?>" alt=""></div>

    </div>
</main>