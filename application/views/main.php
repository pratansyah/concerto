<?php
    $share = "Concerto Tasty Moment ".$title;
    $shareFb = "";
    $shareTwitter = "";
    if($content == "single") {
        $share = $video->title;
        $shareFb = $video->kata_semangat;
        $shareTwitter = $video->rasa_persahabatan;
    }
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta property="og:url" content="<?php echo current_url(); ?>" />
        <meta property="og:title" content="<?php echo $share; ?>" />
        <meta property="og:description" content="<?php echo $shareFb; ?>" />
        <?php if($content == "single"): ?>
            <meta property="og:image" content="<?php echo base_url()."upload/".$video->pictures[0]->filename; ?>" />
        <?php endif; ?>
        <title><?php echo "Concerto Tasty Moment-".$title; ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="<?php assets_url('style.css'); ?>">
        <link rel="stylesheet" href="<?php assets_url('sweet-alert.css'); ?>">
        <link rel="stylesheet" href="<?php assets_url('jquery.powertip.css'); ?>">
        <script type="text/javascript">
            // Global data to be accessed/modified across multiple js files
            var data = new FormData();
            var testing;
            var host = window.location.host;
            var base_url = (host == 'localhost') ? 'http://localhost/concerto/' : 'http://'+host+'/';
            var fb_name, fb_id;
            var fb_photos = [];
            var picture_url = (host == 'localhost') ? 'http://localhost/concerto/upload/' : 'http://'+host+'/upload/';
            var first_run = true;
            var empty_pics = true;
            var total = 0;
            var input = null;
            var current = 0;
            var desktop = null;
            var full = false;
            var mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

            <?php echo ($this->session->userdata('user_id')) ? 'fb_id = '.$this->session->userdata('user_id') : ''; ?>
            <?php echo ($content == "review") ? 'full = true; ': ''; ?>
            <?php echo "var page_title = '$title';" ?>
        </script>
        <script src="<?php assets_url('js/modernizr.js'); ?>"></script>
        <script src="<?php assets_url('js/coverflow.min.js'); ?>"></script>
        <?php if($body_class == "single-gallery" || $body_class == "review-video"): ?>
            <script type="text/javascript">
                <?php
                    echo "var video_cover = '".base_url()."upload/".$video->cover->filename."'; ";
                    echo "var video_description = '".$video->kata_semangat."'; ";
                    echo "var video_title = '".$video->title."'; ";
                    echo "var headline = '".$video->kata_manis."'; ";
                    echo "var pictures = []; ";
                    foreach($video->pictures as $picture) {
                        echo "pictures[".($picture->sequence+1)."] = '$picture->filename'; ";
                    }
                ?>
            </script>
        <?php endif; ?>
    </head>
    <body class="<?php echo $body_class; ?>">
        <div class="soc-med-mbl">
            <a href="http://twitter.com/share?text=<?php echo $share.": ".$shareTwitter ?>&url=<?php echo current_url(); ?>" class="twitter"><img src="<?php assets_url('img/twt-mbl.png'); ?>" alt=""></a>
            <a href="#" class="fb"><img src="<?php assets_url('img/fb-mbl.png'); ?>" alt=""></a>
        </div>
        <?php if($body_class == "create-video instructions overlayed"): ?>
            <div class="overlay"></div>
            <span class="lanjut"><img src="<?php assets_url('img/lanjut.png'); ?>" alt=""></span>
            <div class="fb-album">
                <div class="inner">
                    <div class="row clear">
                        <div class="fb-counter">
                            <span class="fc">0</span>/20
                        </div>
                        <select id="fb-select" onchange="get_photos_albums(this.value);">
                            <option value="*">Pilih Album Lainnya</option>
                        </select>
                    </div>
                    <div id="user-albums" class="row clear frame-container">
                    </div>
                </div>
                <div class="row functional-button">
                    <a href="#" class="button cancel">cancel</a>
                    <a href="#" class="button done">done</a>
                </div>
            </div>
        <?php elseif($body_class == "single-gallery"): ?>
            <div class="overlay"></div>
            <div class="popup-video" style="display: none; background: none;"></div>
        <?php endif; ?>

        <div class="resizer" style="display:none;">
            <div class="hint">
                <!-- <p><strong>Petunjuk:</strong> tahan <span>SHIFT</span> untuk mempertahankan rasio foto.</p> -->
            </div>
            <div class="overlay-croping">
                <div class="overlay-inner">
                </div>
            </div>
            <!-- This image must be on the same domain as the demo or it will not work on a local file system -->
            <!-- http://en.wikipedia.org/wiki/Cross-origin_resource_sharing -->
            <img class="resize-image" src="" alt="image for resizing">
            <div class="functional-button">
                <p>Crop fotomu dengan cara menarik tiap sudut kotak yang ada di foto.</p>
                <div class="row">
                    <button class="button btn-crop js-crop">crop</button>
                    <button class="button cancel">Cancel</button>
                </div>
            </div>
        </div>

        <header>
            <div class="clouds">
                <div class="cloud-1" data-speed="35000"></div>
                <div class="cloud-2" data-speed="45000" data-delay="15000"></div>
                <div class="cloud-3" data-speed="40000"></div>
                <div class="cloud-4" data-speed="38000" data-delay="20000"></div>
            </div>

            <div class="container">


                <a href="<?php echo base_url(); ?>"class="site-logo">
                    tasty moment concerto
                    <img src="<?php assets_url('img/logo.png'); ?>" alt="">
                </a>

                <a class="gallery-btn" href="<?php echo base_url()."view/all"; ?>">gallery</a>

                <div class="campaign-title">
                    <img src="<?php assets_url('img/concerto.png'); ?>" alt="">
                </div>

                <div class="soc-med">
                    <a href="http://twitter.com/share?text=<?php echo $share.": ".$shareTwitter ?>&url=<?php echo current_url(); ?>" class="fb" target="_blank">share twitter</a>
                    <!-- <a href="https://www.facebook.com/dialog/feed?app_id=1516543281944345&display=popup&link=<?php echo current_url(); ?>&redirect_uri=http://www.facebook.com&caption=<?php echo $shareFb; ?>" class="twitter" target="_blank">share fb</a> -->
                    <a id="share-fb" href="#" class="twitter">share fb</a>
                </div>
            </div>
        </header>
        <?php $this->view($content); ?>

        <footer id="footer">
        </footer>

        <script src="<?php assets_url('js/jquery.js'); ?>"></script>
        <script src="<?php assets_url('js/sweet-alert.min.js'); ?>"></script>
        <script src="<?php assets_url('js/jquery.powertip.min.js'); ?>"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.js"></script>
        <script src="<?php assets_url('js/plugins.js'); ?>"></script>
        <script src="<?php assets_url('js/script.js'); ?>"></script>
        <script src="<?php assets_url('js/methods.js'); ?>"></script>
    </body>
</html>