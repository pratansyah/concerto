<div id="content" class="span9">
	<div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">All Submissions</div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    
						<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
						<thead>
							<tr>
								<th>Name</th>
								<th>Video Submitted</th>
								<th>Detail</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($users as $user): if($user->total > 0):?>
								<tr class="odd">
									<td><a target="_blank" href="http://www.facebook.com/<?php echo $user->user_id; ?>"><?php echo $user->name; ?></a></td>
									<td><?php echo $user->total; ?></td>
									<td><a href="<?php echo base_url()."admin/users/detail/".$user->user_id; ?>">View</a></td>
								</tr>
							<?php endif; endforeach; ?>
						</tbody>
					</table>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>