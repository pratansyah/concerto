<div id="content" class="span9">
	<div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">Submissions by <?php echo $video['username']; ?></div>
            </div>
            <div class="block-content collapse in">
                <div class="span12">
                    
						<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
						<thead>
							<tr>
								<th>Title</th>
								<th>Likes</th>
								<th>Created</th>
								<!-- <th>Detail</th> -->
							</tr>
						</thead>
						<tbody>
							<?php foreach($video['submissions'] as $video): ?>
								<tr class="odd">
									<td><a target="_blank" href="<?php echo base_url()."view/single/".$video->video_id; ?>"><?php echo $video->title; ?></a></td>
									<td><?php echo $video->total_likes; ?></td>
									<td class="center"><?php echo date("j M Y g:i", strtotime($video->created)); ?></td>
									<!-- <td><a href="<?php echo base_url().'admin/sumbissions/detail/'.$video->video_id; ?>">View</a></td> -->
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
</div>