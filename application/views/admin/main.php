<!DOCTYPE html>
<html>
    <head>
        <title>Admin</title>
        <!-- Bootstrap -->
        <link href="<?php admin_assets_url('bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php admin_assets_url('bootstrap/css/bootstrap-responsive.min.css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php admin_assets_url('assets/styles.css'); ?>" rel="stylesheet" media="screen">
        <link href="<?php admin_assets_url('assets/DT_bootstrap.css'); ?>" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="<?php admin_assets_url('js/vendor/modernizr-2.6.2-respond-1.1.0.min.js'); ?>"></script>
        <script src="<?php admin_assets_url('vendors/jquery-1.9.1.min.js') ?>"></script>
        <script src="<?php admin_assets_url('bootstrap/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php admin_assets_url('vendors/datatables/js/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php admin_assets_url('assets/DT_bootstrap.js'); ?>"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li <?php echo ($navigation == "submissions") ? "class='active'" : ''; ?>>
                            <a href="<?php echo base_url()."admin/submissions"; ?>"><i class="icon-chevron-right"></i> Submissions</a>
                        </li>
                        <li <?php echo ($navigation == "users") ? "class='active'" : ''; ?>>
                            <a href="<?php echo base_url()."admin/users"; ?>"><i class="icon-chevron-right"></i> Users</a>
                        </li>
                    </ul>
                </div>
                
                <?php $this->view("admin/$content"); ?>
            </div>
        </div>
    </body>

</html>