(function($, document, window){
	"use strict";
	
	function equalizeColumnContentHeight(){
		if( window.innerWidth > 990 ){
			$(".equal-height").each(function(){
				var height = $(this).innerHeight();
				$(this).find(".col-content").height(height);
			});
		} else {
			$(".equal-height").each(function(){
				$(this).find(".col-content").height("auto");
			});
		}
	}

	// Document ready
	$(document).ready(function(){

		// Cloning main navigation for mobile menu
		$(".mobile-navigation").append( $(".main-navigation .menu").clone() );

		// Makin mobile menu behave like an accordion
		$(".menu-item-has-children").click(function(){

			$(this).toggleClass("active");
			$(this).children('.sub-menu').slideToggle();

    		return false;
		});

		// Mobile menu toggle 
		$(".menu-toggle").click(function(){
			$(".mobile-navigation").slideToggle(300);
		});

		// Changing background image using data-attribute
		$("[data-bg-image]").each(function(){
			var image = $(this).data("bg-image");
			$(this).css("background-image", "url("+image+")");
		});

		// Changing background color using data-attribute
		$("[data-bg-color]").each(function(){
			var color = $(this).data("bg-color");
			$(this).css("background-color", color );
		});

		equalizeColumnContentHeight();

	});

	// Window resizing event
	$(window).resize(function(){

		equalizeColumnContentHeight();

	});


	// Window loaded event
	$(window).load(function(){

	});

})(jQuery, document, window);