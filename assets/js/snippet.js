/*	+ File ini tidak perlu di panggil;
	+ File ini berisi kumpulan kode-kode jquery snippet yang siap pakai, gunakan seperlunya saja; sesuaikan dengan kebutuhan
*/

	// PlaceHolder For TextArea
		$("'[placeholder]'").parents("'form'").submit(function() {
			$(this).find("'[placeholder]'").each(function() {
				var input = $(this);
				if (input.val() == input.attr("'placeholder'")) {
					input.val("''");
				}
			})
		});

	// Scroll Element
		$(window).scroll(function(){
			var ScreenHeight = document.documentElement.clientHeight; // Untuk mengetahui tinggi layar
			var scrollLenght = $(window).scrollTop();

			if( scrollLenght < 50 ){// Jika scrollTop lebih kecil dari 50
				// tulis code action disini
				// Misal
					$("selectornya").addClass('nama-class');
			};

			if( scrollLenght > 50 ){// Jika scrollTop lebih besar dari 50
				// tulis code action disini
				// Misal
					$("selectornya").addClass('nama-class');
			};
		});

	// Menambahkan action pada lebar layar yang ditentukan
		if (document.documentElement.clientWidth < 870) {// Jika lebar layar lebih kecil dari 870
			
			// Action disini

		};

	// Mengatur lebar elemen li navigation agar posisinya selalu pas
		// menghitung lebar element nav yang idnya #nav
		var navWrapWidt = $("#site-navigation").width();

		// Menghitung jumlah li pertama di dalam #site-navigation
		var totalLi = $("#site-navigation div ul#menu-primary > li").length;

		// menentukan lebar li dalam px, hasil dari navWrapWidt dibagi jumlah li
		var liwidth = navWrapWidt / totalLi;

		// menentukan lebar li dalam percent, hasil dari navWrapWidt dibagi jumlah li
		var liwidthPercent = 100 / totalLi;

		// memilih elemen li didalam nav, lalu menambahkan css "width" dengan nilai "liwidth" dan "max-width" dangan nilai liwidthPercent
		$("#site-navigation div ul#menu-primary > li").css({
			"width" : liwidth,
			"max-width" : liwidthPercent + "%"
		});

	//

	// Drop-down menu 
		$( "#menu li" ).has( "ul" ).addClass( "parent" ); // Deteksi children, bila ada children menu tambahkan class `parent` pada parent menu

		// Efek slide untuk Drop-down
			$( "#menu li" ).hover(function(){
		        $(this).addClass( "hover" );
		        $(this).find( "ul:first" ).slideToggle( "medium" );
		    }, function(){
		        $(this).removeClass( "hover" );
		        $(this).find( "ul:first" ).slideUp( "medium" );
		    });

	// Deteksi class
	if( $( "selector" ).hasClass( "class" ) ) { 
    	// action ketika class terdeteksi
    };
    // Deteksi Ipad
    var isiPad = navigator.userAgent.match(/iPad/i) != null;
    
    if(isiPad){

       // alert("just Ipad");

    }