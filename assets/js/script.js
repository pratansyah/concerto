var $ = jQuery.noConflict();
$(document).ready(function(){


	// $('.splash').plaxify()
	// $.plax.enable()


	/*
	=========================================================
						HEADER
	=========================================================
	*/

	if ($(window).width() > 767) {
		$('.soc-med, .gallery-btn').velocity({ translateY: "25px" }, { duration: 2000, loop: true }, [ 0.17, 0.67, 0.83, 0.67 ]); 
		function moveClouds(e, s, d) {
		    $(e).css('left', '-20%');
		        var wait = window.setTimeout(function(){
		            $(e).animate ({
		                left: '120%'
		            }, s, 'linear', function() {
		            moveClouds(e, s, d);
		        });
		    },d);
		}

		if(!Modernizr.cssanimations) {
		    var clouds = [1,2,3,4];
				    
		    $.each(clouds, function() {
		        var e = $('.cloud-' + this);
		        moveClouds(e, e.data('speed'), e.data('delay'));
		    });
		}
	}
	
	$('.strawberry').velocity({ rotateZ: "-15deg" }, { duration: 500, loop: true }, [ 0.17, 0.67, 0.83, 0.67 ]); 



	/*
	=========================================================
						HOME
	=========================================================
	*/
	
	$('.wrap1 .ice.one').velocity({ left: "82px", height: "282px" },{ duration: 600})
	$('.wrap1 .ice.x').velocity({ top: "-70px", height: "80px", left: "90px" },{ duration: 600}).velocity({
		translateY: "-20px"
	},{ duration: 300}).velocity({
		translateY: "-10px"
	},{ duration: 300})
	$('.wrap1 .splash1').velocity({ opacity: 1, rotateZ:"130deg", translateY: "20px", translateX: "-40px" },{ duration: 400, delay: 1200})
	$('.wrap1 .splash2').velocity({ opacity: 1, rotateZ:"0", translateY: "-38px", translateX: "-20px" },{ duration: 400, delay: 1100})
	$('.wrap1 .splash3').velocity({ opacity: 1, translateY: "-28px", translateX: "30px" },{ duration: 400, delay: 1000})



	$('.wrap2 .ice.two').velocity({ left: "10px", height: "261px" },{ duration: 600})
	$('.wrap2 .ice.x').velocity({ top: "-57px", height: "95px", left: "10px" },{ duration: 600}).velocity({
		translateY: "-20px"
	},{ duration: 300}).velocity({
		translateY: "-10px"
	},{ duration: 300})
	$('.wrap2 .splash1').velocity({ opacity: 1, rotateZ:"-20deg", translateY: "-40px", translateX: "-20px" },{ duration: 400, delay: 1000})
	$('.wrap2 .splash2').velocity({ opacity: 1, rotateZ:"-20deg", translateY: "-30px", translateX: "-50px" },{ duration: 400, delay: 1200})
	$('.wrap2 .splash3').velocity({ opacity: 1, rotateZ:"-20deg", translateY: "0px", translateX: "-60px" },{ duration: 400, delay: 1100})


	$('.wrap3 .ice.three').velocity({ left: "175px", top: "-10px", height: "197px" },{ duration: 600})
	$('.wrap3 .ice.x').velocity({ top: "-57px", height: "85px", left: "180px" },{ duration: 600}).velocity({
		translateY: "-20px"
	},{ duration: 300}).velocity({
		translateY: "-10px"
	},{ duration: 300})
	$('.wrap3 .splash1').velocity({ opacity: 1, rotateZ:"-20deg", translateY: "10px", translateX: "90px" },{ duration: 400, delay: 1000})
	$('.wrap3 .splash2').velocity({ opacity: 1, rotateZ:"-20deg", translateY: "30px", translateX: "100px" },{ duration: 400, delay: 1100})
	$('.wrap3 .splash3').velocity({ opacity: 1, rotateZ:"-20deg", translateY: "60px", translateX: "90px" },{ duration: 400, delay: 1200})



	/*
	=========================================================
						CREATE VIDEO
	=========================================================
	*/

	setTimeout(function() {
		$('.lanjut').velocity("transition.shrinkIn");
	}, 1000);
	$('.lanjut').click(function(event) {
		$('.instruction, .btn').velocity("transition.fadeOut", { stagger: 100 });
		setTimeout(function() {
		$('.overlay').velocity({
			scale: 0,
			duration: 600
		});
		},500);
		setTimeout(function() {
			$('body').removeClass('overlayed instructions');
		},1200);
		$(this).fadeOut();
	});
	$('.tutorial').click(function(event) {
		$('body').addClass('instructions overlayed');
		$('.overlay').velocity({
			scale: 2,
			duration: 600
		});
		setTimeout(function() {
			$('.instruction, .btn').velocity("transition.fadeIn", { stagger: 100 });
		},500);
		setTimeout(function() {
			$('.lanjut').velocity("transition.shrinkIn");
		},2000);
	});

	$(function() {

		if ($.fn.reflect) {
			$('.photos .cover').reflect();
		}
		$('.photos').coverflow({
			duration:		'slow',
			index:			3,
			width:			200,
			height:			150,
			visible:		'density',
			selectedCss:	{	opacity: 1	},
			outerCss:		{	opacity: .1	},
			
			confirm:		function() {
				console.log('Confirm');
			},

			select:		function(event, cover) {
				var img = $(cover).children().andSelf().filter('img').last();
				$('#photos-name').text(img.data('name') || 'unknown');
			}
			
		});	
	});

	$('.resizer .cancel').click(function(event) {
		$('.resizer').css('display', 'none');
	});
	$('.fb-album .cancel').click(function(event) {
		$('body').removeClass('overlayed');
		$('body .overlay').velocity({scale: 0, display: "none", delay: 500});
		$('.fb-album').velocity("transition.slideDownBigOut",{duration: 500}, [ 0.17, 0.67, 0.83, 0.67 ])

	});

	$('.fb-upload').click(function(event) {
		if(!fb_id) {
			swal({
				title: "Belum login",
				text: "Silahkan login terlebih dahulu di halaman utama",
				type: "warning",
				confirmButtonColor: "#f03b22",
				confirmButtonText: "Ke halaman utama"
			},
			function(){
				window.location.href = base_url;
			}
			);
		}
		else {
			$('body').addClass('overlayed');
			$('body .overlay').velocity({scale: 2, display: "block"});
			$('.fb-album').velocity("transition.slideDownBigIn",{duration: 500, delay: 500, visibility: "visible" }, [ 0.17, 0.67, 0.83, 0.67 ])
		}
	});

	$('.strawberry').velocity({ rotateZ: "-15deg" }, { duration: 500, loop: true }, [ 0.17, 0.67, 0.83, 0.67 ]); 


	$('.frame-fb').click(function(event) {
		// $(this).children('.inner').removeClass('overlay');
		$(this).toggleClass('hidden selected');
	});

	$('.overlay').click(function(event) {
		setTimeout(function() {
			$('body').removeClass('overlayed');
			$('.overlay').fadeOut();
		},600);
		fullScreen = false;
		$('.popup-video').css('display', 'none');
		$('.popup-video').html(' ');
		
	});

	
	$(".frame-container").mCustomScrollbar({
		theme:"minimal"
	});




	/*
	=========================================================
						GALLERY
	=========================================================
	*/
	$('.search-field').focusin(function(event) {
		$(this).parent().width(180);
	});
	$('.search-field').focusout(function(event) {
		$(this).parent().width(130);
	});

	

	$(".arrow-top").hide();
	$(window).scroll(function(){
		var ScreenHeight = document.documentElement.clientHeight; // Untuk mengetahui tinggi layar
		var scrollLenght = $(window).scrollTop();
		if( scrollLenght > 300 ){
			$('.scroll').fadeOut();
		};
		if( scrollLenght == 0 ){
			$('.scroll').fadeIn();
		};
	});
	
});


// Window loaded event
$(window).load(function(){
	var $container = $('.isotope').isotope({
		itemSelector: '.thumbnail-gallery',
		layoutMode: 'fitRows'
	});
	var filterFns = {
		numberGreaterThan50: function() {
		var number = $(this).find('.number').text();
		return parseInt( number, 10 ) > 50;
	},
	ium: function() {
		var name = $(this).find('.name').text();
		return name.match( /ium$/ );
		}
	};
	$('#filters').on( 'change', function() {
		var filterValue = this.value;
		filterValue = filterFns[ filterValue ] || filterValue;
		$container.isotope({ filter: filterValue });
	});
});


