var $ = jQuery.noConflict();

$(document).ready(function() {
	function readURL(inputImage) {
		if (inputImage.files && inputImage.files[0]) {
			// $('.photos').empty();
			$('.resizer').css('display', 'block');
			total = inputImage.files.length;
			input = inputImage;
			desktop = true;
			resizeableImage($('.resize-image'));
			first_run = false;
		}
	}

	$("#userPhotos").change(function() {
		readURL(this);
	});

	$("#userPhotos").click(function(e){
		if(!fb_id) {
			swal({
				title: "Belum login",
				text: "Silahkan login terlebih dahulu di halaman utama",
				type: "warning",
				confirmButtonColor: "#f03b22",
				confirmButtonText: "Ke halaman utama"
			},
			function(){
				window.location.href = base_url;
			}
			);
			e.preventDefault();
		}
	});

	$(".create-video .button.next").click(function(){
		// e.preventDefault();
		if($(this).hasClass('tutorial')) return;
		if(!fb_id) {
			swal({
				title: "Belum login",
				text: "Silahkan login terlebih dahulu di halaman utama",
				type: "warning",
				confirmButtonColor: "#f03b22",
				confirmButtonText: "Ke halaman utama"
			},
			function(){
				window.location.replace(base_url);
			}
			);
		}
		else {
			title = $("#title").val();
			kata_manis = $("#kata_manis").val();
			rasa_persahabatan = $("#rasa_persahabatan").val();
			kata_semangat = $("#kata_semangat").val();

			if(title == '' || kata_manis == '' || rasa_persahabatan == '' || kata_semangat == '' || !data.notEmpty || empty_pics) {
				var warningText = "Silahkan lengkapi data sebelum submit";
				if(title == '') warningText += "\n Judul";
				if(kata_manis == '') warningText += "\n Kata-kata manis";
				if(rasa_persahabatan == '') warningText += "\n Rasa persahabatan";
				if(kata_semangat == '') warningText += "\n Kata penyemangat";
				if(!data.notEmpty || empty_pics) warningText += "\n Foto";
				swal({
					title: "Data tidak lengkap",
					text: warningText,
					type: "warning",
					confirmButtonColor: "#f03b22"
				});
			}
			else {
				data.append('title', title);
				data.append('kata_manis', kata_manis);
				data.append('rasa_persahabatan', rasa_persahabatan);
				data.append('kata_semangat', kata_semangat);
				$.ajax({
					url: base_url+'ajax/upload/',
					data: data,
					processData: false,
					contentType: false,
					type: 'POST',
					dataType: 'json',
					success: function(result){
						window.location.replace(base_url+"create/review/"+result.video_id);
					}
				});
			}
		}
	});


	$('.fb-album .done').click(function(event) {
		// $('.photos').empty();
		total = fb_photos.length;
		desktop = false;
		resizeableImage($('.resize-image'), first_run);
		first_run = false;

		$('body').removeClass('overlayed');
		$('body .overlay').velocity({scale: 0, display: "none", delay: 500});
		$('.fb-album').velocity("transition.slideDownBigOut",{duration: 500}, [ 0.17, 0.67, 0.83, 0.67 ]);
		$('.resizer').css('display', 'block');
	});

    window.addEventListener('message', fromIframe, false);
    function fromIframe(event) {
    if(!isNaN(event.data) && event.data == 583) {
			$('body').addClass('overlayed');
			$('.popup-video').css('display', 'block');
			full = true;
			$('.popup-video').html('<iframe src="'+base_url+'video/video_maker.html" width="640" height="360" style="border: none;"></iframe>')
        }
    }

    $("#share-fb, .fb-share").click(function(){
    	FB.ui({
			method: 'feed',
			appid: 1516543281944345,
			link: window.location.href,
			display:'popup',
			caption: 'Concerto bold tasty moment',
			name: (typeof video_title !== 'undefined') ? video_title : page_title,
			picture: (typeof video_cover !== 'undefined') ? video_cover : base_url+"assets/img/concerto.png",
			description: (typeof video_description !== 'undefined') ? video_description : " "

		}, function(response){});
    });

    $("#userPhotos").powerTip({
    	placement: 'n'
    });
    $(".fb-upload").powerTip({
    	placement: 'n'
    });

    $("#filters").change(function() {
    	$("#search-form").submit();
    });

    $('.likes').click(function(event) {
    	// console.log($(this).data('video-id'));
    	var item = $(this);
    	$.ajax({
			url: base_url+'ajax/like/',
			data: {video_id: $(this).data('video-id')},
			type: 'POST',
			dataType: 'json',
			success: function(result){
				if(result.status == true) {
					console.log(result.status);
					item.addClass('liked');
					item.html(Number(item.html())+1);
				}
			}
    	});
    });
});

function arrayHasOwnIndex(array, prop) {
    return array.hasOwnProperty(prop) && /^0$|^[1-9]\d*$/.test(prop) && prop <= 4294967294; // 2^32 - 2
}

window.fbAsyncInit = function() {
	FB.init({
	  appId      : '1516543281944345',
	  status 	 : true,
	  cookie     : true,
	  xfbml      : true,
	  version    : 'v2.2'
	});
	$(window).triggerHandler('fbAsyncInit');
	//jQuery(document).trigger('FBSDKLoaded');

	FB.Event.subscribe('auth.authResponseChange', function(response) {

	 	 	if (response.status === 'connected')
		  	{
		  		console.log("connected to facebook")
		  		//SUCCESS

		  	}
			else if (response.status === 'not_authorized')
		    {
		    	console.log("failed connected to facebook")
				//FAILED
		    } else
		    {
		    	console.log("error");
		    	//UNKNOWN ERROR
		    }
	});

};

(function(d, s, id){
	 var js, fjs = d.getElementsByTagName(s)[0];
	 if (d.getElementById(id)) {return;}
	 js = d.createElement(s); js.id = id;
	 js.src = "//connect.facebook.net/en_US/sdk.js";
	 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

/*(function($) {
    $(document).bind('FBSDKLoaded', function() {
    	check_login_state();
        get_user_photos();
    });

})(jQuery);*/

$(window).bind('fbAsyncInit', function() {
  	check_login_state();
  	/*setTimeout(function(){
  		get_user_photos();
  	},3000);*/
    //get_user_photos();
});

function login_facebook(){
	FB.login(function (response){
		if (response.authResponse) {
				//console.log("access token "+response.authResponse.accessToken);
				var accessToken = response.authResponse.accessToken;
				FB.api('/me', function (response){
					fb_name = response.name;
					fb_id = response.id;
					$.ajax({
						url: "ajax/login/"+fb_id+"/"+fb_name+"/"+accessToken,
						type: "GET",
						dataType: "json",
						success:
							function(response){
								// console.log(response);
								//if(response.status) window.location.replace(base_url+"create/");
								if (response.status) {
									if(mobile) window.location.href = "view/all";
									else window.location.href = "create";
								};
							},
						error:
							function(){
								console.log("failed to login");
							}
					});

				})
		}else{
			console.log("login failed");
		}
	}, { scope: 'email,user_photos,publish_actions' });
}

function check_login_state(){
	var toreturn = "";
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            window.userId =  response.authResponse.userID;
        	toreturn = "GetLoginStatus: logged in" + " " + response.authResponse.accessToken;

        	//get_user_photos();
        	get_user_albums();
        } else {
        	toreturn = "GetLoginStatus: Logged Out";
        }
        console.log(toreturn);
    });
}

function get_user_photos(){
	console.log("get user photos");
	FB.api(
		    "/me/photos/uploaded",
		    function (response) {
		    	console.log(response);
		      if (response && !response.error) {
		        //var j = JSON.parse(response);
		        var data = response.data;
		        $('.photos').empty();
		        for (var i = 0; i < data.length; i++) {
		        	console.log(data[i].source);
		        	$('.photos').append('<img class="cover" src="'+data[i].source+'"/>');
		        };

		      }
		    }
		);
}

function get_user_albums(){
	FB.api(
			"/me/albums",
			function (response){
				if (response && !response.error) {
					var data = response.data;
					//console.log(data);
					for (var i = 0; i < data.length; i++) {
						//console.log(data[i].id+" "+data[i].name);
						$('#fb-select').append("<option value='"+data[i].id+"'>"+data[i].name+"</option>")
						//get_photos_albums(data[i].id);
					};
				}
			}
		);
}


function get_photos_albums(album_id){
	var html = "";
	$('#mCSB_1_container').html("");
	FB.api(
			"/"+album_id+"/photos",
			function (response){
				if (response && !response.error) {
					var data = response.data;
					//console.log(data);
					for (var i = 0; i < data.length; i++) {
						var img = data[i].images;
						//console.log(img);
						// console.log(img[img.length-1].source);
						// console.log(img[0].source);
						html += '<div id="'+(new Date).getTime()+fb_id+Math.floor(Math.random() * 10000000000000001)+'" class="col-4 frame-fb" data-fullsize="'+img[0].source+'">\
					                            <div class="inner overlay-frame">\
					                                <figure>\
					                                    <img width="188" height="141" src="'+img[img.length-1].source+'" alt="">\
					                                </figure>\
					                            </div>\
					                        </div>';
					};
					$('#mCSB_1_container').html(html);

					$('.frame-fb').click(function(event) {
						console.log($(this).hasClass('selected'));
						if($(this).hasClass('selected')) {
							$("span.fc").html(Number($("span.fc").html())-1);
							fb_photos.splice(fb_photos.indexOf($(this).data('fullsize')), 1);
							$(this).removeClass('hidden selected');
						}
						else {
							$("span.fc").html(Number($("span.fc").html())+1);
							fb_photos.push($(this).data("fullsize"));
							$(this).addClass('hidden selected');
						}
						// $(this).toggleClass('hidden selected');
					});
					$(".frame-container").mCustomScrollbar({
						theme:"minimal"
					});
				}
			}
		);
}




