create table user(
	user_id varchar(25) primary key,
	name varchar(100)
);

create table video(
	video_id int(10) primary key auto_increment,
	user_id varchar(25),
	title varchar(50),
	kata_manis varchar(50),
	kata_semangat varchar(50),
	rasa_persahabatan varchar(50)
);

create table pictures(
	picture_id int(10) primary key auto_increment,
	filename varchar(40),
	video_id int(10),
	sequence int(3)
);

create table likes(
	video_id int(10),
	user_id varchar(25)
);